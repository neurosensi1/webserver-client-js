var PORT = 2000;
//var HOST = '193.61.181.26';
//var HOST = '193.61.179.9';
var HOST = '127.0.0.1'
//var HOST = '107.34.126.192'

var dgram = require('dgram');
//var message = new Buffer('My KungFu is Good!');
var client = dgram.createSocket('udp4');

var buff = new Buffer(64);

var prevTime = new Date().getTime();
var currTime;

const readline = require('readline');
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

function interval(duration, fn){
  this.baseline = undefined
  
  this.run = function(){
    if(this.baseline === undefined){
      this.baseline = new Date().getTime()
    }
    fn()
    var end = new Date().getTime()
    this.baseline += duration
 
    var nextTick = duration - (end - this.baseline)
    if(nextTick<0){
      nextTick = 0
    }
    (function(i){
        i.timer = setTimeout(function(){
        i.run(end)
      }, nextTick)
    }(this))
  }

this.stop = function(){
   clearTimeout(this.timer)
 }
}


function sendData(){

	//currTime = new Date().getTime();
	//console.log(currTime - prevTime)
	//prevTime = currTime;
	buff.writeDoubleLE(Math.random(), 0);
	buff.writeDoubleLE(Math.random(), 8);
	buff.writeDoubleLE(Math.random(), 16);
	//buff.writeDoubleLE(Math.random(), 24);
	//buff.writeDoubleLE(Math.random(), 32);
	//buff.writeDoubleLE(Math.random(), 40);
	//buff.writeDoubleLE(Math.random(), 48);
	//buff.writeDoubleLE(Math.random(), 56);

	client.send(buff, 0, buff.length, PORT, HOST, function(err, bytes) {
	    if (err) throw err;
	    //console.log('UDP message sent to ' + HOST +':'+ PORT);
	    //console.log(buff);
	    //client.close();
	});
}

//setInterval(sendData, 8);
var timer = new interval(8, sendData);
timer.run()
console.log("Simulating Data ......");

rl.question('Press Enter to stop simulation', (answer) => {
  // TODO: Log the answer in a database
  //console.log("Pressed Neter");
  var stop = '0000'
  console.log(stop.length)
  client.send(stop, 0, stop.length, PORT, HOST, function(err, bytes) {
	    if (err) throw err;
	    //console.log('UDP message sent to ' + HOST +':'+ PORT);
	    //console.log(buff);
	    //client.close();
	});
  timer.stop();
  //client.close();
  rl.close();
  //process.exit();
});