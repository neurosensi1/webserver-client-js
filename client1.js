#!/usr/bin/env node

/*UDP Server to receive incoming data*/

var PORT = 3002;       //UDP server running on port
var HOST = '0.0.0.0'; //UDP server address

var dgram = require('dgram');
var server = dgram.createSocket('udp4');

var io = require('socket.io-client');
var serverUrl = 'http://localhost:9000/ingestintoDB';
var conn = io.connect(serverUrl);

var buffLength = 20;
var brainData_buff = new Array(buffLength);

var timeLabel;
var deltaTime = 8;

var tagUser = {'userId': 1, 'sessionId': 1};
var precision = {precision : 'ms'};
var ChannelData = {'time':0, 'channel1': 0, 'channel2': 0, 'channel3': 0};
var firstTime = true;
//var point = [{'channel1': 0, 'channel2': 0, 'channel3': 0}, {'userId': 1, 'sessionId': 1}];

conn.on('connect', function () { console.log("Websocket connected"); });

var count = 0;

function sendData(brainData){

    conn.emit('brainData', brainData);

}

server.on('listening', function () {
    var address = server.address();
    console.log('UDP Server listening on ' + address.address + ":" + address.port);
});

server.on('message', function (message, remote) {
    var i = 0;
    //console.log("buffer at the starrt: ", brainData_buff);
    if(firstTime){
        timeLabel = new Date().getTime();
        firstTime = false;
        console.log("Receiving Data ....");
    }

    for(var key in ChannelData){
        if(key != 'time'){
            ChannelData[key] = message.readDoubleLE(i);
            i = i + 8;
        }
        else{
            
            ChannelData[key] = timeLabel;
            timeLabel = timeLabel + deltaTime;
        }
    }

    i = 0;

    //point = [ChannelData, tagUser]
    //console.log("ChanelData: ", [ChannelData, tagUser]);
    //console.log("buffer before adding: ", brainData_buff);
    brainData_buff[count] = JSON.parse(JSON.stringify([ChannelData, tagUser]));
    //console.log("buffer after adding: ", brainData_buff);
    count = count + 1;
    //console.log(brainData_buff);

    if(count == buffLength){
        //console.log(brainData_buff);
        sendData(brainData_buff);
        count = 0;
    }
});


server.bind(PORT, HOST);
