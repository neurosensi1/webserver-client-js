#!/usr/bin/env node

/*UDP Server to receive incoming data*/

var PORT = 3002;       //UDP server running on port
var HOST = '0.0.0.0'; //UDP server address

var PORT_GAME = 3010;
var HOST_GAME = '0.0.0.0';

var dgram = require('dgram');
var server = dgram.createSocket('udp4');
var server_Game = dgram.createSocket('udp4');

var io = require('socket.io-client');
var request = require('request');
var serverUrl = 'http://193.61.190.50:9000/ingestintoDB';
//var serverUrl = 'http://52.31.154.40:7000/ingestintoDB';
var conn = io.connect(serverUrl);

var buffLength = 20;
var brainData_buff = new Array(buffLength);

var timeLabel;
var deltaTime = 8;

var timeLabel_Game;
var deltaTime_Game = 16;
var buffLength_Game = 10;
var gameData_buff = new Array(buffLength_Game);

var tagUser = {'userId': 1, 'sessionId': 1, 'Chapter': 1, 'Level': 1, 'Difficulty': 1};
var precision = {precision : 'ms'};
var ChannelData = {'time':0, 'channel1': 0, 'channel2': 0, 'channel3': 0};

var tagGame = {'Chapter': 1, 'Level': 1, 'Difficulty': 1}
var gameData = {'time':0, 'channel4': 0, 'channel5': 0, 'channel6': 0};

var firstTime = true;
var firstTime_game = true;
//var point = [{'channel1': 0, 'channel2': 0, 'channel3': 0}, {'userId': 1, 'sessionId': 1}];

conn.on('connect', function () { console.log("Websocket connected"); });

var count = 0;
var count_Game = 0;

function sendData(brainData){

    conn.emit('brainData', brainData);

}

function sendData_Game(gameData){

    conn.emit('gameData', gameData);

}


server.on('listening', function () {
    var address = server.address();
    console.log('UDP Server 1 listening on ' + address.address + ":" + address.port);
});

server.on('message', function (message, remote) {
    var i = 0;
    //console.log("buffer at the starrt: ", brainData_buff);
    if(firstTime && !firstTime_game){
        timeLabel = timeLabel_Game
        firstTime = false;
        console.log("Receiving Brain Data ....");
    }

    else if(firstTime && firstTime_game){
        timeLabel = new Date().getTime()
        /*request('http://52.31.154.40:7000/getTime', function (error, response, body) {
            if (!error && response.statusCode == 200) {
                //console.log(body) // Show the HTML for the Google homepage. 
                timeLabel = body;
            }
            else{
                console.log("error");
            }
        })*/
        firstTime = false;
        console.log("Receiving Brain Data ....");
    }

    for(var key in ChannelData){
        if(key != 'time'){
            ChannelData[key] = message.readDoubleLE(i);
            //console.log(ChannelData[key]);
            i = i + 8;
        }
        else{       
            ChannelData[key] = timeLabel;
            timeLabel = timeLabel + deltaTime;
        }
    }

    i = 0;
    brainData_buff[count] = JSON.parse(JSON.stringify([ChannelData, tagUser]));
    count = count + 1;

    if(count == buffLength){
        //console.log("chanel full");
        sendData(brainData_buff);
        count = 0;
    }
});

server_Game.on('listening', function () {
    var address = server_Game.address();
    console.log('UDP Server 2 listening on ' + address.address + ":" + address.port);
});

server_Game.on('message', function (message, remote) {

    var i = 0;
    //if(message.readUInt8(0) == 1)   console.log("1");
    //else if(message.readUInt8(0) == 2)  console.log("2");

    if(firstTime_game && !firstTime){
        timeLabel_Game = timeLabel;
        firstTime_game = false;
        console.log("Receiving Game Data ....");
    }

    else if(firstTime_game && firstTime){
        timeLabel_Game = new Date().getTime()
        /*request('http://52.31.154.40:7000/getTime', function (error, response, body) {
            if (!error && response.statusCode == 200) {
                //console.log(body) // Show the HTML for the Google homepage. 
                timeLabel_Game = body;
            }
            else{
                console.log("error");
            }
        })*/
        firstTime_game = false;
        console.log("Receiving Game Data ....");
    }

    for(var key in gameData){
        if(key != 'time'){
            gameData[key] = message.readFloatLE(i);
            //console.log(message.readFloatLE(i));
            i = i + 4;
        }
        else{
            gameData[key] = timeLabel_Game;
            timeLabel_Game = timeLabel_Game + deltaTime_Game;
        }
    }
    //ChannelData['channel4'] = message.readUInt8(i)
    i = 0;
    //if(gameData['channel4'] == 1 || gameData['channel4'] == 2){
    gameData_buff[count_Game] = JSON.parse(JSON.stringify([gameData, tagUser, precision]));
    count_Game = count_Game + 1;
   //}

    if(count_Game == buffLength_Game){
        //console.log("game full")
        sendData_Game(gameData_buff);
        count_Game = 0;
    }

});


server.bind(PORT, HOST);
server_Game.bind(PORT_GAME, HOST_GAME);
